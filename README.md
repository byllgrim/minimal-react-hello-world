# Minimal React Hello World

This is a very minimal tutorial/demo for how to use
[React](https://en.wikipedia.org/wiki/React_(software)).

It will give you a "Hello World" page.

No need for `create-react-app` or any other stupid dependencies.


## Dependencies

* [npm](https://en.wikipedia.org/wiki/Npm)


## Usage

Run `make`, or just `npm install && npm start`.

Use your web browser to open http://localhost:8080/.

You can copy this tutorial/demo as a starting point for your own React apps.

--------

(If you know how to make this even simpler, please let me know.)
