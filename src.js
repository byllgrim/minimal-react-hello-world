import React from 'react';
import ReactDOM from 'react-dom';

const rootElement = document.getElementById('root');

ReactDOM.render(<h1>Hello, world!</h1>, rootElement)
