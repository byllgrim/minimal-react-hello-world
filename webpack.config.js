const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
  mode: "development",
  plugins: [
    new HtmlWebpackPlugin({
      template: "./index.html"
    })
  ],
  module: {
    rules: [
      {
        test: /\.js$/,
        use: "babel-loader",
      },
    ],
  }
};
